import loader
import messages
from logger import Print

BACKENDS_PATH = 'backends'

class Backend:
    def __init__(self, nickname):
        self.nickname = nickname

    def set_config(self, config):
        self.config = config

    def init(self):
        """
        config is a dictionnary with all the config options
        specific to the backend present in config.py file
        """
        pass

    def close(self):
        pass

    def receive_message(self, message, sender, private=False):
        messages.receive_message(message, sender, private)

    def receive_special(self):
        messages.receive_special()

    def send_message(self, message, to):
        pass


backend = None

def load_backend(bot_nickname, backend_name, config):
    Print.header("Loading backend")
    global backend
    loaded_backends = loader.load_classes_from_modules(BACKENDS_PATH, Backend)
    Print.debug("loaded backends list :", loaded_backends)
    try:
        backend = loaded_backends[backend_name](bot_nickname)
    except KeyError:
        Print.error_and_exit("Backend", backend_name, "does not exist !")
    backend.set_config(config)
    Print.info(backend_name, "loaded")
