
import argparse
import config

parser = argparse.ArgumentParser(
    description='''A chat bot that brings fun in your life''',
)


parser.add_argument('-d',
                    '--debug',
                    help='debug output, implies -v',
                    action='store_true')
parser.add_argument('-v',
                    '--verbose',
                    help='verbose output',
                    action='store_true')
parser.add_argument('-p',
                    '--plugins',
                    help='Activate plugins',
                    nargs='+',
                    metavar='PLUGIN')

args = parser.parse_args()

if args.debug :
    config.LOGDEBUG = True
if args.verbose or config.LOGDEBUG :
    # if debug is activated, either with argument or config, force verbose
    config.LOGVERBOSE = True

if args.plugins :
    for p in args.plugins:
        if p not in config.PLUGINS:
            config.PLUGINS.append(p)

