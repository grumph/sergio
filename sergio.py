#!/bin/env python3

import arguments_manager
import config
import plugin
import backend
import loader
from logger import Print

def load_config(prefix, specific_config = None):
    loaded_config = loader.load_variables(config, prefix)
    if loaded_config and specific_config:
        if specific_config in loaded_config:
            loaded_config = loaded_conf[specific_config]
        else:
            loaded_config = {}
    return loaded_config


def launch_bot():
    plugins_config = load_config('PLUGIN_')
    plugin.load_plugins(config.BOTNAME, config.PLUGINS, plugins_config)

    backend_name = config.BACKEND
    backend_config = load_config('BACKEND_', backend_name)
    backend.load_backend(config.BOTNAME, backend_name, backend_config)

    b = backend.backend

    try:
        Print.header("Starting backend")
        b.init()
    except KeyboardInterrupt:
        Print.info("keyboard interrupt received")
        for p in plugin.plugins:
            p.close()
        b.close()


if __name__ == "__main__":
    launch_bot()
