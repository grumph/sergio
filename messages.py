#!/bin/env python
from logger import Print
import plugin
import backend

def receive_message(message, sender, private=False):
    """Manage any message coming
    if message_is_command:
     - send command to appropriate command managers
    else:
      - send message to analysers (ex: stats plugin)
      if a plugin took the lead:
        - send the message to it
      else :
        - send messages to message managers
    """
    message = message.lstrip()
    if message.startswith(plugin.COMMAND_PREFIX):
        # there is at least COMMAND_PREFIX in message, index 0 is always valid
        command_name = message.split()[0].lstrip(plugin.COMMAND_PREFIX)
        if command_name in plugin.commands:
            command = plugin.commands[command_name]["callback"]
            params = message[len(command_name) + 1:].lstrip()
            command(params)
        else:
            Print.warning("Unknown command", command_name)
            send_message("Unknown command " + command_name, sender, True)
            send_message("Available: " + str(list(plugin.commands.keys())), sender, True)
    else:
        for p in plugin.plugins:
            p.check_incoming_message(message, sender, private)
        for p in plugin.plugins:
            p.manage_message(message, sender, private)

def receive_special():
    """Manage special events (somebody entering/leaving)
    and send it to plugins managing that special event
    """
    pass

def send_message(message, receiver, system_message):
    """Manage messages to be sent
     - send message to sender modifiers
     - send it to the backend
    """
    if not system_message:
        for p in plugin.plugins:
            message = p.modify_sent_message(message)
    backend.backend.send_message(message, receiver)
