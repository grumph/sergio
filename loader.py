

from sys import exc_info
import imp
import inspect
from pathlib import Path
from logger import Print

def load_classes_from_modules(path, class_type):
    Print.debug("load_classes_from_modules", path, class_type)
    p = Path(path)
    modules_files = list(p.glob("*.py"))
    found_classes = {}
    for module_file in modules_files:
        module_name = module_file.stem
        try:
            # TODO : There is a bug, if a filename in path is the same as one
            #        of the core bot filename, the different files are imported
            #        in the same module. A quick and dirty fix is to add a string
            #        at the begining of the module name. It's working as long as none of
            #        the bot's core file begins with that pattern.
            module = imp.load_source('_%s_%s' % (class_type.__name__, module_name), str(module_file.absolute()))
            is_class_from_module = lambda x: inspect.isclass(x) and inspect.getmodule(x) is module
            all_classes = inspect.getmembers(module, is_class_from_module)
            Print.debug('classes from', module_name, ':', all_classes)
            selected_classes = [x for (_, x) in all_classes
                                if x is not class_type
                                and class_type in type.mro(x)]
            if len(selected_classes) > 1:
                raise Exception("Found more than one " + str(class_type) +
                                " in module " + module_name)
            for c in selected_classes:
                found_classes[module_name] = c
        except:
            e = exc_info()
            Print.warning('Could not load module', module_name)
            Print.debug(str(e))
    return found_classes


def load_variables(module, prefix):
    Print.debug("load_variables", module, prefix)
    res = {}
    for var_name, value in inspect.getmembers(module):
        if var_name.startswith(prefix):
            res[var_name.lstrip(prefix)] = value
    return res
