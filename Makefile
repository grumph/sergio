





all:
	python sergio.py


clean:
	rm -rf sergio.log.*
	find . -name __pycache__ -print | xargs rm -rf
	find . -name '#*#' -print | xargs rm -rf
