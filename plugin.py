import messages
import loader
from logger import Print
PLUGINS_PATH='plugins'
COMMAND_PREFIX='!'

class Plugin:
    help = 'This plugin has no help message.'
    activable = False

    def __init__(self, bot, config):
        self.bot = bot
        self.config = config
        self.activated = True

    def get_commands(self):
        return {}

    def activate(self, set_to_on=True):
        self.activated = set_to_on

    def plugin_callback(self, params):
        if params:
            first = params.split()[0].lower()
            if "on" == first:
                self.activated = False
            elif "off" == first:
                self.activated = True

    def check_incoming_message(self, message, sender, private):
        pass

    def manage_message(self, message, sender, private):
        pass

    def modify_sent_message(self, message):
        return message

    def send_message(self, message, to, system=False):
        messages.send_message(message, to, system)

    def close(self):
        pass

plugins = []
commands = {}

def register_commands(plugin):
    Print.debug("Register commands for", plugin.__module__)
    commands_to_register = plugin.get_commands()
    already_registered_commands = set(commands).intersection(set(commands_to_register))
    for c in already_registered_commands:
        Print.warning("Command", c, "from", plugin.__module__,
                      "will override", c, "from", commands[c]["callback"].__module__)
    commands.update(commands_to_register)

def plugin_activation(plugin_name, plugin):
    if plugin.activable:
        commands[plugin_name] = {"callback": plugin.plugin_callback,
                                 "help": plugin.help_message}

def load_plugins(bot_nickname, plugins_list, plugins_config):
    Print.header("Loading plugins")
    loaded_plugins = loader.load_classes_from_modules(PLUGINS_PATH, Plugin)
    for p in plugins_list:
        try:
            config = plugins_config[p] if p in plugins_config else None
            Print.debug(bot_nickname, config)
            plugin = loaded_plugins[p](bot_nickname, config)
            register_commands(plugin)
            plugin_activation(p, plugin)
            plugins.append(plugin)
            Print.info(p, "loaded")
        except KeyError as e:
            Print.error('Could not load plugin', p)
    Print.debug("loaded plugins :", plugins)
