# -*- coding: utf-8 -*-
#!/usr/bin/env python


################################################################################
# Library for printing and logging
################################################################################


import sys
import logging
import os
import shutil
import time
import config
import inspect

###############################################################################
# Variables

DEBUG = config.LOGDEBUG
VERBOSE = config.LOGVERBOSE
LOGFILE = config.LOGFILE
ROTATE_LOGFILE = config.LOGROTATE


LOG_STDOUT = 21 # logger.INFO is 20
LOG_STDERR = 22

###############################################################################
# Print class

class Print:
    ENDC = '\033[0m'
    BLUE='\033[1;34m' #reminder
    LIGHT_BLUE='\033[0;34m'
    RED='\033[0;31m'
    GREEN='\033[0;32m'
    YELLOW='\033[1;33m'
    LIGHT_RED='\033[1;31m'
    WHITE='\033[1;37m'

    def args_to_text(args):
        text = ''
        for t in args:
            text += str(t) + ' '
        return text[:-1]

    @classmethod
    def header(self, *args):
        text = self.args_to_text(args)
        logging.info('*'*80)
        logging.info('* ' + text)
        logging.info('*'*80)
        print(self.BLUE, text, self.ENDC, sep='')

    @classmethod
    def info(self, *args):
        text = self.args_to_text(args)
        logging.info(text)
        print("[" + self.YELLOW + "INFO" + self.ENDC + '] ' + text)

    @classmethod
    def warning(self, *args):
        text = self.args_to_text(args)
        logging.warning(text)
        print("[" + self.LIGHT_RED + "Warning" + self.ENDC + '] ' + text + self.ENDC, file=sys.stderr)

    @classmethod
    def debug(self, *args):
        text = self.args_to_text(args)
        logging.debug(text)
        stack = inspect.stack()[1]
        calling_module = stack[0].f_code.co_filename
        calling_func = stack[3]
        if "<module>" != calling_func:
            calling_module += " in " + calling_func
        if DEBUG:
            print("[" + self.LIGHT_BLUE + "DEBUG" + self.ENDC + "][" + calling_module + "] " + text)

    @classmethod
    def error(self, *args):
        text = self.args_to_text(args)
        logging.error(text)
        print(self.RED + "ERROR : " + text + self.ENDC, file=sys.stderr)

    @classmethod
    def error_and_exit(self, *args):
        text = self.args_to_text(args)
        logging.critical(text)
        print(self.RED + "ERROR : " + text + self.ENDC, file=sys.stderr)
        exit(1)

    @classmethod
    def stderr(self, *args):
        text = self.args_to_text(args)
        logging.log(LOG_STDERR, text)
        if VERBOSE:
            sys.stderr.write(text)

    @classmethod
    def stdout(self, *args):
        text = self.args_to_text(args)
        logging.log(LOG_STDOUT, text)
        if VERBOSE:
            sys.stdout.write(text)


###############################################################################
# Init logger

logging.addLevelName(LOG_STDOUT, 'STDOUT')
logging.addLevelName(LOG_STDERR, 'STDERR')

handler = None
try:
    if ROTATE_LOGFILE and os.path.exists(LOGFILE):
        shutil.move(LOGFILE, LOGFILE + '.' + str(int(time.time())))
    handler = logging.FileHandler(LOGFILE, "a")
except:
    raise IOError("Couldn't create/open file \"" + \
                  LOGFILE + "\". Check permissions.")
logging.getLogger().setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
handler.setFormatter(formatter)
logging.getLogger().addHandler(handler)


###############################################################################
# Unit tests

if __name__ == "__main__" :
    Print.header("Starting the printing tests")
    Print.info("for your information, it's working")
    Print.debug("I also handle debugs")
    Print.warning("there is no problem here")
    Print.stderr("this may be an error (stderr)\n")
    Print.stdout("this is not an error (stdout)\n")
    Print.error("let's try an error")
    Print.error_and_exit("And now we exit not properly")
