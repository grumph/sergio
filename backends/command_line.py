#import readline
from backend import Backend
import readline

class CommandBackend(Backend):
    def init(self):
        try:
            while True:
                message = input("You > ")
                self.receive_message(message, "You", private=True)
        except EOFError:
            self.close()

    def send_message(self, message, to):
        print(self.nickname, ">", message)

    def close(self):
        print("Bye bye")
