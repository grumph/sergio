import backend
import sleekxmpp

class XMPPBackend(backend.Backend):
    def init(self):
        self.bot = sleekxmpp.ClientXMPP(self.config['jid'], self.config['password'])
        b = self.bot
        b.add_event_handler('session_start', self._start)
        b.add_event_handler('groupchat_message', self._muc_message)
        b.register_plugin('xep_0030') # Service Discovery
        b.register_plugin('xep_0045') # Multi User Chat
        b.register_plugin('xep_0199') # XMPP Ping

    def _start(self, event):
        self.bot.send_presence()
        self.bot.get_roster()
        self.bot.plugin['xep_0045'].joinMUC(self.config['room'],
                                            self.config['nick'],
                                            password=self.config['room_password'],
                                            wait=True)

    def muc_message(self, msg):
        if msg['mucnick'] != self.nickname:
            self.receive_message(msg['body'], msg['mucnick'])


    def send_message(self, message, to):
        # Send message on the chat
        pass

    def close(self):
        # Disconnect
        pass
