
from logger import Print
import plugin

class CoreCommands(plugin.Plugin):
    def get_commands(self):
        quit_command = {"callback": self.quit_bot,
                         "help": "Kill the bot. You can add his last words as a parameter."}
        return {"quit": quit_command,
                "exit": quit_command,
                "help": {"callback": self.get_help,
                         "help": ("List the available commands.\n" +
                                  plugin.COMMAND_PREFIX + "help <command>:" +
                                  "help for given command.")}}

    def quit_bot(self, params):
        if params:
            self.send_message(params, None)
        Print.info("Exit command received, sending KeyboardInterrupt")
        raise KeyboardInterrupt


    def get_help(self, params):
        p = params.split()
        if p:
            try:
                message = ("Help for " + plugin.COMMAND_PREFIX + p[0] + ": " +
                           plugin.commands[p[0]]["help"])
            except KeyError:
                message = "Command " + p[0] + "does not exist or has no help message."
        else:
            message = ""
            for c in plugin.commands:
                message += plugin.COMMAND_PREFIX + c + ' '
        self.send_message(message, None, True)
