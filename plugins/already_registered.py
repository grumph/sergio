
from plugin import Plugin
from logger import Print


class AlreadyRegisteredPlugin(Plugin):
    def get_commands(self):
        return {"quit": {"callback": self.quit_bot,
                         "help": "Kill the bot. You can add his last words as a parameter."}}

    def quit_bot(self, params):
        if params:
            self.send_message(params, None)
        Print.info("Exit command received, sending KeyboardInterrupt")
        raise KeyboardInterrupt
