
from plugin import Plugin


class L33tPlugin(Plugin):
    def modify_sent_message(self, message):
        res = message
        res = res.replace('A', '4')
        res = res.replace('a', '4')
        res = res.replace('B', '8')
        res = res.replace('b', '8')
        res = res.replace('E', '3')
        res = res.replace('e', '3')
        res = res.replace('I', '1')
        res = res.replace('i', '1')
        res = res.replace('o', '0')
        res = res.replace('O', '0')
        res = res.replace('T', '7')
        res = res.replace('t', '7')
        res = res.replace('S', '5')
        res = res.replace('s', '5')
        res = res.replace('Z', '2')
        res = res.replace('z', '2')
        return res
