
from plugin import Plugin


class MirrorPlugin(Plugin):
    help_message = "The mirror plugin will make the bot repeat what you said."
    activable = True

    def manage_message(self, message, sender, private):
        # play with the config
        if 'prefix' in self.config:
            message = self.config['prefix'] + message
        # send the same message to the backend
        self.send_message(message, sender)
